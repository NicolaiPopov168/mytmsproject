package com.example.mytms

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.navArgs
import com.example.mytms.databinding.SecondFragmentBinding

class SecondFragment : BaseFragment<SecondFragmentBinding>() {

    private val args: SecondFragmentArgs by navArgs()

    override fun createViewBinding(
        inflater: LayoutInflater,
        container: ViewGroup?
    ): SecondFragmentBinding = SecondFragmentBinding.inflate(inflater, container, false)

    override fun SecondFragmentBinding.onBindView(savedInstanceState: Bundle?) {
        textView.setOnClickListener{
            navController.popBackStack()
        }

        println("args.fragmentId = ${args.fragmentId}")
        println("args.fragmentId = ${args.fragmentId}")
    }


}